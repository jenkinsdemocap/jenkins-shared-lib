import hudson.model.*
import jenkins.model.*
import hudson.slaves.*
import hudson.slaves.EnvironmentVariablesNodeProperty.Entry
import hudson.plugins.sshslaves.SSHLauncher
import hudson.plugins.sshslaves.verifiers.*

def call(def host) {


	def j = Jenkins.getInstance()

	SshHostKeyVerificationStrategy hostKeyVerificationStrategy = new NonVerifyingKeyVerificationStrategy()

	ComputerLauncher launcher = new SSHLauncher(
			host, // Host
			22, // Port
			"jenkins-agent", // Credentials
			(String)null, // JVM Options
			(String)null, // JavaPath
			(String)null, // Prefix Start Slave Command
			(String)null, // Suffix Start Slave Command
			(Integer)null, // Connection Timeout in Seconds
			(Integer)null, // Maximum Number of Retries
			(Integer)null, // The number of seconds to wait between retries
			hostKeyVerificationStrategy // Host Key Verification Strategy
	)

	// Define a "Permanent Agent"
	Slave agent = new DumbSlave(
			host,
			"/home/jenkins",
			launcher)

	agent.nodeDescription = "Agent node description"
	agent.numExecutors = 1
	agent.labelString = "agent-node-label"
	agent.mode = Node.Mode.NORMAL
	agent.retentionStrategy = new RetentionStrategy.Always()

	j.addNode(agent)
}
